#GNU Awk

Gawk is the GNU version of the commonly available UNIX awk program, another popular stream editor.
Since the awk program is often just a link to gawk, we will refer to it as awk.

The basic function of awk is to search files for lines or other text units containing one or more patterns. When
a line matches one of the patterns, special actions are performed on that line.

#Formatting fields
awk '{ print "Size is " $5 " bytes for " $9 }'
Size is 160 bytes for orig

df -h | sort -rnk 5 | head -3 | awk '{ print "Partition " $6 "\t: " $5 " full!" }'
Partition /var : 86% full!
Partition /usr : 85% full!

Below another example where we search the /etc directory for files ending in ".conf" and starting with
either "a" or "x", using extended regular expressions:

from /etc> ls -l | awk '/\<(a|x).*\.conf$/ { print $9 }'

amd.conf
antivir.conf
xcdroast.conf

This example illustrates the special meaning of the dot in regular expressions: the first one indicates that we
want to search for any character after the first search string, the second is escaped because it is part of a string
to find (the end of the file name).

The END statement can be added for inserting text after the entire input is processed:

> ls -l | \
awk '/\<[a|x].*\.conf$/ { print $9 } END { print \
"Can I do anything else for you, mistress?" }'

The field separator is represented by the built-in variable FS.

awk 'BEGIN { FS=":" } { print $1 "\t" $5 }' /etc/passwd
kelly Kelly Smith
franky Franky B.
eddy Eddy White
willy William Black
cathy Catherine the Great
sandy Sandy Li Wong


pg 82

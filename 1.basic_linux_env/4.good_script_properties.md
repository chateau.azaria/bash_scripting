#Properties of a good script

1 - A script should run without errors.
2 - It should perform the task for which it is intended
3 - Program logic is clearly defined and apparent
4 - A script does not do unnecessary work
5 - Scripts should be reusable

#Structure

When starting a new script, ask yourself the following questions:


- Will I be needing any information from the user or from the user's env?
- How will I store that information?
- Are there any files to be created? (Permissions and Ownership)
- What commands will I use?
- Does the user need any notifications?


